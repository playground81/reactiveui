﻿using ReactiveUIProva.Views;
using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ReactiveUIProva
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();

            new AppBootstrapper();  //registra i service
            MainPage = new MainPage();
        }

        protected override void OnStart()
        {
            // HomePage
            App.Current.MainPage = new NavigationPage(new LoginPage());
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }
    }
}
