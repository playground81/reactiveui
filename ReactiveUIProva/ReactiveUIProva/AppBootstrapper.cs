﻿using ReactiveUIProva.Services;
using System;
using System.Collections.Generic;
using System.Text;

namespace ReactiveUIProva
{
    public class AppBootstrapper
    {
        public AppBootstrapper()
        {
            RegisterServices();
        }

        private void RegisterServices()
        {
            Splat.Locator.CurrentMutable.Register(() => new StaticContactsService(), typeof(IContactsService));
        }
    }
}
