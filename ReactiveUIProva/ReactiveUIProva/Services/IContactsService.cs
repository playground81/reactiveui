﻿using ReactiveUIProva.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace ReactiveUIProva.Services
{
    public interface IContactsService
    {
        IEnumerable<Contact> GetAllContacts();

    }

    public class StaticContactsService : IContactsService
    {
        private static List<Contact> _contactsList = new List<Contact>()
            {
                new Contact{FullName = "Elisa", Email = "elisa@email", Phone = "456735633"},
                new Contact{FullName = "Mirko", Email = "mirko@email", Phone = "54678456"},
                new Contact{FullName = "Daniele", Email = "daniele@email", Phone = "4567455678"},
                new Contact{FullName = "Elena", Email = "elena@email", Phone = "73567337"},
                new Contact{FullName = "Area51", Email = "Area51@email", Phone = "23452345"}
            };

        public IEnumerable<Contact> GetAllContacts()
        {
            return _contactsList;
        }
    }
}
