﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ReactiveUIProva.Models
{
    public class ChartData
    {
        public int ColorNum { get; set; }
        public string ColorPos { get; set; }
    }
}
