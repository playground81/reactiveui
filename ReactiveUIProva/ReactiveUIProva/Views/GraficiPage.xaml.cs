﻿using ReactiveUIProva.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ReactiveUIProva.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class GraficiPage : ContentPage
    {
        public GraficiViewModel _viewModel;
        public GraficiPage(int red, int green, int blue)
        {
            InitializeComponent();

            BindingContext = _viewModel = new GraficiViewModel(red, green, blue);


        }
    }
}