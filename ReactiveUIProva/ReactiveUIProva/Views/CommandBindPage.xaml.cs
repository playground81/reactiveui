﻿using ReactiveUI;
using ReactiveUI.XamForms;
using ReactiveUIProva.ViewModels;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ReactiveUIProva.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class CommandBindPage : ReactiveContentPage<CommandBindViewModel>    //using ReactiveUI.XamForms;
    {
        public CommandBindPage()
        {
            InitializeComponent();

            //ViewModel viene ereditato da ReactiveContentPage<CommandBindViewModel>
            BindingContext = ViewModel = new CommandBindViewModel();

            //TestCommand è definito nel CommandBindViewModel
            //slider è definito nella page.xaml
            this.BindCommand(ViewModel, vm => vm.TestCommand, page => page.slider, nameof(slider.ValueChanged));
        }
    }
}