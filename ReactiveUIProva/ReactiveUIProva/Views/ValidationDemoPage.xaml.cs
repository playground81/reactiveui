﻿using ReactiveUI;
using ReactiveUI.Validation.Extensions;
using ReactiveUI.XamForms;
using ReactiveUIProva.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ReactiveUIProva.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ValidationDemoPage : ReactiveContentPage<ValidationDemoViewModel>
    {
        public ValidationDemoPage()
        {
            InitializeComponent();

            BindingContext = ViewModel = new ValidationDemoViewModel();

            //per essere sicuro che le istruzioni all'interno vengano eseguite dopo che tutti i componenti e le variabili sono stati inizializzati
            this.WhenActivated(d =>
            {
                this.BindValidation(ViewModel, vm => vm.Birthdate, page => page.validationLabel.Text);

            });
        }
    }
}