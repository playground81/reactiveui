﻿using ReactiveUI;
using ReactiveUIProva.Views;
using System;
using System.Collections.Generic;
using System.Reactive;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace ReactiveUIProva.ViewModels
{
    class HomePageViewModel : ReactiveObject
    {
        //.Ctors
        public HomePageViewModel()
        {
            SelectColorCommand = ReactiveCommand.CreateFromTask(SelectColorAsync);
        }

        #region Properties

        private int _Red;
        public int Red
        {
            get => _Red;
            set
            {
                //Utilizziamo questo metodo offerto da ReactiveUI invece di implementare il PropertyChanged direttamente da INotifyPropertyChanged
                this.RaiseAndSetIfChanged(ref _Red, value);
                //Questo metodo è come se facesse Invoke del PorpertyChanged che notifica gli observer
                this.RaisePropertyChanged(nameof(BackgroundColor));
            }
        }

        private int _Green;
        public int Green
        {
            get => _Green;
            set
            {
                this.RaiseAndSetIfChanged(ref _Green, value);
                this.RaisePropertyChanged(nameof(BackgroundColor));
            }
        }

        private int _Blue;
        public int Blue
        {
            get => _Blue;
            set
            {
                this.RaiseAndSetIfChanged(ref _Blue, value);
                this.RaisePropertyChanged(nameof(BackgroundColor));
            }
        }

        //Voglio che il valore di questa property cambi ogni volta che cambia il valore di una delle 3 di cui sopra
        public Color BackgroundColor
        {
            get => Color.FromRgb(Red, Green, Blue);
        }
        #endregion

        #region Commands - Tasks
        public ReactiveCommand<Unit, Unit> SelectColorCommand { get; }
        private async Task SelectColorAsync()
        {
            Device.BeginInvokeOnMainThread(async () =>
            {
                await App.Current.MainPage.Navigation.PushAsync(new GraficiPage(Red, Green, Blue));
            });
        }


        #endregion
    }
}
