﻿using ReactiveUI;
using ReactiveUIProva.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;
using System.Reactive.Linq;
using ReactiveUIProva.Services;

namespace ReactiveUIProva.ViewModels
{
    public class ContactsViewModel : ReactiveObject
    {
        private IContactsService _contactsService;

        public ContactsViewModel(IContactsService contactsService = null)
        {
            contactsService = contactsService ?? (IContactsService)Splat.Locator.Current.GetService(typeof(IContactsService));
            var allContacts = contactsService.GetAllContacts();

            Contacts = new ObservableCollection<Contact>(allContacts);

            //si sottoscrive al cambiamento di SearchQuery e ogni volta che gli viene notificato ricalcola Contacts
            this.WhenAnyValue(vm => vm.SearchQuery)
                .Throttle(TimeSpan.FromSeconds(1))  //se prima di un secondo scrivi un altro carattere, aspetta a far partire il filtro (aggiunge un ritardo)
                .Subscribe(query =>
                {
                    var filteredContacts = allContacts.Where(el => el.FullName.ToLower().Contains(query)
                                            || el.Email.ToLower().Contains(query) || el.Phone.ToLower().Contains(query)).ToList();

                    Contacts = new ObservableCollection<Contact>(filteredContacts);
                });

            /*
             * ObservableAsPropertyHelper<string> _searchResult prende il valore che viene ritornato nella Select qui sotto,
             * al variare di almeno uno tra gli AnyValue, e viene assegnato al ToProperty specificato sotto.
             * E' più veloce di fare una property con get e set, tanto viene settata solo al change dell'AnyValue
             * e ti da anche la garanzia che nessun altro farà mai set di questa proprietà
             */
            this.WhenAnyValue(vm => vm.Contacts)
                .Select(contacts =>
                {
                    if (Contacts.Count == allContacts.Count())
                        return "No filters applied";

                    return $"{Contacts.Count} contacts have been found in result for '{SearchQuery}'";
                })
                .ToProperty(this, vm => vm.SearchResult, out _searchResult);

        }

        //Properties - ObservableAsPropertyHelper

        private readonly ObservableAsPropertyHelper<string> _searchResult;
        public string SearchResult { get => _searchResult.Value; }


        private string _SearchQuery = "";
        public string SearchQuery
        {
            get => _SearchQuery;
            set
            {
                this.RaiseAndSetIfChanged(ref _SearchQuery, value);
            }
        }

        private ObservableCollection<Contact> _Contacts;
        public ObservableCollection<Contact> Contacts
        {
            get => _Contacts;
            set
            {
                this.RaiseAndSetIfChanged(ref _Contacts, value);   
            }
        }
    }
}
