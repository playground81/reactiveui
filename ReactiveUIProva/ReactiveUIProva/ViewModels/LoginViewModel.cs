﻿using ReactiveUI;
using ReactiveUI.Fody.Helpers;
using ReactiveUIProva.Views;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Reactive;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace ReactiveUIProva.ViewModels
{
    class LoginViewModel : ReactiveObject, IScreen
    {
        //.Ctors
        public LoginViewModel()
        {
            var canExecuteLogin = this.WhenAnyValue(
                           vm => vm.Username,
                           vm => vm.Password,
                           (usr, pwd) => !string.IsNullOrEmpty(Username) && !string.IsNullOrEmpty(Password));

            //il primo parametro è la funzione da eseguire
            //il secondo è il "canExecute" e serve a disattivare il comando se restituisce false
            LoginCommand = ReactiveCommand.CreateFromTask(LoginAsync, canExecuteLogin);

            GoToContactsCommand = ReactiveCommand.CreateFromTask(GoToContactsAsync);

            GoToTestCommandBind = ReactiveCommand.CreateFromTask(GoToTestAsync);

            GoToValidationCommand = ReactiveCommand.CreateFromTask(GoToValidationAsync);

            //serve per gestire le eccezioni nei commands
            GoToContactsCommand.ThrownExceptions.Subscribe(ex => 
            {
                Debug.WriteLine(ex.Message);
            });



            Router = new RoutingState();
        }

        //Routing
        public RoutingState Router { get; }

        //Commands - Tasks
        public ReactiveCommand<Unit, Unit> LoginCommand { get; }
        public ReactiveCommand<Unit, Unit> GoToContactsCommand { get; }
        public ReactiveCommand<Unit, Unit> GoToTestCommandBind { get; }
        public ReactiveCommand<Unit, Unit> GoToValidationCommand { get; }
        private async Task LoginAsync()
        {
            if (!string.IsNullOrEmpty(Username) &&
                Username == "Admin" &&
                !string.IsNullOrEmpty(Password) &&
                Password == "12345")
            {
                //le azioni che coinvolgono l'interfaccia sono da eseguire sempre sul Main Thread
                Device.BeginInvokeOnMainThread(async () =>
                {
                    await App.Current.MainPage.Navigation.PushAsync(new HomePage());
                });
            }
            else
            {
                Username = "Admin";
                Password = "12345";
            }
        }

        private async Task GoToContactsAsync()
        {
            Device.BeginInvokeOnMainThread(async () =>
            {
                await App.Current.MainPage.Navigation.PushAsync(new ContactsPage());
            });
        }

        private async Task GoToTestAsync()
        {
            Device.BeginInvokeOnMainThread(async () =>
            {
                await App.Current.MainPage.Navigation.PushAsync(new CommandBindPage());
            });
        }

        private async Task GoToValidationAsync()
        {
            Device.BeginInvokeOnMainThread(async () =>
            {
                await App.Current.MainPage.Navigation.PushAsync(new ValidationDemoPage());
            });
        }

        //Properties
        private string _Username;
        public string Username
        {
            get => _Username;
            set
            {
                this.RaiseAndSetIfChanged(ref _Username, value);
                this.RaisePropertyChanged(nameof(IsVisibleError));  //chiede a IsVisibleError di rifare la get
                //this.RaisePropertyChanged(nameof(IsBtnEnabled));  //chiede a IsBtnEnabled di rifare la get -- non è più necessaria perchè uso la canExecute
            }
        }

        private string _Password;
        public string Password
        {
            get => _Password;
            set
            {
                this.RaiseAndSetIfChanged(ref _Password, value);
                this.RaisePropertyChanged(nameof(IsVisibleError));
                //this.RaisePropertyChanged(nameof(IsBtnEnabled));
            }
        }
        
        public bool IsVisibleError
        {
            get => string.IsNullOrEmpty(Username) || string.IsNullOrEmpty(Password);
        }

        //ATTENZIONE: SE METTIAMO LA "canExecute" NEL COMMAND, CI PENSA LEI A ATTIVARE/DISATTIVARE IL BUTTON, QUINDI QUESTA PROPERTY NON è PIù NECESSARIA!
        //private bool _IsBtnEnabled = false;
        //public bool IsBtnEnabled
        //{
        //    get => !string.IsNullOrEmpty(Username) && !string.IsNullOrEmpty(Password);
        //    set => this.RaiseAndSetIfChanged(ref _IsBtnEnabled, value);
        //}
    }
}
