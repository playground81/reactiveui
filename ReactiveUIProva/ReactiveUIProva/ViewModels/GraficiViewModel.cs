﻿using ReactiveUI;
using ReactiveUI.Fody.Helpers;
using ReactiveUIProva.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Drawing;
using System.Text;

namespace ReactiveUIProva.ViewModels
{
    public class GraficiViewModel : ReactiveObject
    {
        [Reactive] public int Red { get; set; }
        [Reactive] public int Green { get; set; }
        [Reactive] public int Blue { get; set; }

        private ObservableCollection<ChartData> _Data;

        public ObservableCollection<ChartData> Data
        {
            get => _Data;
            set => this.RaiseAndSetIfChanged(ref _Data, value);
        }

        public Color ColorRGB { [ObservableAsProperty] get; }

        public GraficiViewModel() { }

        public GraficiViewModel(int red, int green, int blue)
        {
            Data = new ObservableCollection<ChartData>();

            Red = red;
            Green = green;
            Blue = blue;

            Data.Add(new ChartData { ColorNum = red, ColorPos = "Red"});
            Data.Add(new ChartData { ColorNum = green, ColorPos = "Green" });
            Data.Add(new ChartData { ColorNum = blue, ColorPos = "Blue" });

        } 

    }
}
