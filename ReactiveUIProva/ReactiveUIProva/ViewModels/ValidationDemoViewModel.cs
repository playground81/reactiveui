﻿using ReactiveUI;
using ReactiveUI.Validation.Abstractions;
using ReactiveUI.Validation.Contexts;
using ReactiveUI.Validation.Extensions;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Windows.Input;

namespace ReactiveUIProva.ViewModels
{
    public class ValidationDemoViewModel : ReactiveObject, IValidatableViewModel
    {
        public ValidationContext ValidationContext { get; } = new ValidationContext();

        private DateTime _Birthdate;
        public DateTime Birthdate
        {
            get => _Birthdate;
            set
            {
                this.RaiseAndSetIfChanged(ref _Birthdate, value);
            }
        }

        public ICommand SubmitCommand { get; }

        public ValidationDemoViewModel()
        {
            this.ValidationRule(vm => vm.Birthdate,
                            value => value > new DateTime(1970, 1, 1) &&
                            value < new DateTime(2010, 12, 31), "Birthday should be a valid date!");

            var isValid = this.IsValid();

            SubmitCommand = ReactiveCommand.Create(() =>
            {
                Debug.WriteLine($"{Birthdate} submitted!");
            }, isValid);    //isValid è la canExecute

        }

    }
}
